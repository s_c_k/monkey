// components/nav-no-search/index.js
const App = getApp();
Component({
  /**
   * 组件的属性列表
   */
	
  properties: {
		item: String,
		StyleItem: String
  },
	options: {
		addGlobalClass: true,
		multipleSlots: true
	},
	lifetimes: {
		attached: function() {
			this.setData({
			  navH: App.globalData.navHeight,
			  btnTop: App.globalData.btnTop,
			  btnHeight: App.globalData.btnHeight
			})
		}
	},
  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})
