//app.js
var http = require("utils/http.js");
App({
  onLaunch: function () {
    const self = this;
		wx.getSystemInfo({
			success: function (res) {
				self.globalData.systemInfo = res
			}
		})
    wx.getSystemInfo({
      success: res => {
        //导航高度
        let model = res.model.slice(0, 6)
        if (model == 'iphone') {
          self.globalData.navHeight = res.statusBarHeight + 44;
        } else {
          self.globalData.navHeight = res.statusBarHeight + 48;
        }
				self.globalData.winHeight = res.windowHeight
      }, fail(err) {
        console.log(err);
      }
    }),
    self.globalData.btnTop = wx.getMenuButtonBoundingClientRect().top
    self.globalData.btnHeight = wx.getMenuButtonBoundingClientRect().height
  },
  globalData: {
    // 定义全局请求队列
    requestQueue: [],
    // 是否正在进行登陆
    isLanding: true,
    // 购物车商品数量
    totalCartCount: 0,
    systemInfo: {},
    navHeight: 0,
    btnTop: 0,
    btnHeight: 0,
		winHeight: 0,
		userInfo: {}
  }
})