/*
 * Copyright (c) 2018-2999 武汉三只猴子科技有限公司 All rights reserved.
 *
 * https://www.3monkeys.shop/
 *
 * 未经允许，不可做商业用途！
 *
 * 版权所有，侵权必究！
 */

package com.monkey.shop.sys.constant;

/**
 * 常量
 */
public class Constant {
	/** 超级管理员ID */
	public static final int SUPER_ADMIN_ID = 1;

	/** 系统菜单最大id */
	public static final int SYS_MENU_MAX_ID = 1;

}
