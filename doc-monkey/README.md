
#您有一条新的消息

         当热爱学习，思考的你看到这条消息的时候，那么首先恭喜你，即将获得的是一个优秀并且完全开源的小程序商城系统,
     具体你想怎么用，秉承着开源的原则，想怎么使用，怎么开心怎么来！不多啰嗦，直接带你了解代码吧

#1.目录介绍：
 ```
    doc-monkey           #操作文档
    monkey-shop-admin    #后台管理
    monkey-shop-api      #小程序后台
    monkey-shop-bean     #基础实体
    monkey-shop-common   #基础工具
    monkey-shop-quartz   #定时任务
    monkey-shop-security #登录授权
    monkey-shop-service  #业务服务
    monkey-shop-sys      #系统设置
    monkeyViewApp        #小程序代码
    monkeyViewVue        #vue代码
    LICENSE              #开源协议
    README.md            #项目说明
  ```

#2.技术概览：

     | 技术                   | 版本   | 说明                                    |
     | ---------------------- | ------ | ---------------------------------------|
     | Spring Boot            | 2.1.6  | MVC核心框架                             |
     | Spring Security oauth2 | 2.1.5  | 认证和授权框架                          |
     | MyBatis                | 3.5.0  | ORM框架                                |
     | MyBatisPlus            | 3.1.0  | 基于mybatis，使用lambda表达式的         |
     | Swagger-UI             | 2.9.2  | 文档生产工具                            |
     | Hibernator-Validator   | 6.0.17 | 验证框架                                |
     | redisson               | 3.10.6 | 对redis进行封装、集成分布式锁等          |
     | hikari                 | 3.2.0  | 数据库连接池                            |
     | log4j2                 | 2.11.2 | 更快的log日志工具                       |
     | fst                    | 2.57   | 更快的序列化和反序列化工具               |
     | orika                  | 1.5.4  | 更快的bean复制工具                      |
     | lombok                 | 1.18.8 | 简化对象封装工具                        |
     | hutool                 | 4.5.0  | 更适合国人的java工具集                  |
     | swagger-bootstrap      | 1.9.3  | 基于swagger，注解形式生成在线接口文档    |
     | mysql5.7               | 5.7    | 分布式数据库                            |
     | java1.8                | 1.8    | springboot最低入门版本                  |
     | redis                  | 3.2    | 缓存 好东西，反正面试的时候都会问的深入   |
    你还要了解 小程序 + vue  + 七牛云sdk + 微信支付sdk + 项目环境安装 + 服务部署    |
    
#3.后端需要修改的配置文件汇总

- 3.1推荐使用idea，安装lombok插件后，使用idea导入maven项目
 ```
     打开 idea > File > settings > Plugins 搜索 lombok
  ```

- 3.2将monkey_shop.sql导入到mysql中，修改`application-dev.yml`更改 datasource.url、user、password
 ```
     如果版本是mysql5.7以下，请修改字段属性json为text，否则执行会报错
  ```

- 3.3修改后端微信小程序信息 monkey-shop-ma中ma.properties:
 ```
    mp.appid=小程序id
    mp.secret=小程序秘钥
  ```

- 3.4修改`monkey-shop-ma中mp.properties` 修改微信公众号信息
 ```
    mp.appid=****
    mp.secret=****
    mp.token=****
    mp.aesKey=****
  ```

- 3.5修改`monkey-shop-ma中pay.properties` 修改微信支付信息
 ```
    pay.mchId=商户id
    pay.mchKey=8ZKK3monkeysf3monkeysd3monkeysl8
    #pay.keyPath=classpath:*.p12  #如果支付方式换成其他的则去需要去微信商户平台下载秘钥文件
  ```

- 3.6修改`monkey-shop-ma中shop.properties` 修改七牛云、阿里大于等信息,我们用的七牛云免费10G空间
 ```
    shop.qiniu.resourcesUrl=七牛云配置的域名
    shop.qiniu.accessKey=七牛云-个人中心-秘钥管理中获取key(有两个，都可以用)
    shop.qiniu.secretKey=七牛云-个人中心-秘钥管理中获取key(有两个，都可以用)
    shop.qiniu.bucket=七牛云空间名字
    #shop.qiniu.zone=HUA_DONG    #可根据七牛云空间所在地区配置,也可以配置,代表用hash算法获取,效率更高
    #shop.aLiDaYu.accessKeyId=
    #shop.aLiDaYu.accessKeySecret=
    #shop.aLiDaYu.signName=
    shop.tokenAesKey=*******   #如果七牛云空间是私有的则需要在七牛云那里去获取秘钥
  ```

- 3.7修改`monkey-shop-api中api.properties` 当前接口所在域名，用于支付回调，从而订单变为支付成功的状态
```
    api.datacenterId=1
    api.workerId=1
    api.domainName=https://www.applet.3monkeys.shop/notice/pay/order  #微信支付回调地址
  ```

- 3.8修改`monkey-shop-madmin中api.properties` 为正式环境后台接口的域名（admin项目的域名），否则无法收到微信退款回调，从而订单变为退款成功的状态
```
    admin.datacenterId=1
    admin.workerId=0
    admin.domainName=http://admin.3monkeys.shop
  ```

# 4 .关于本地启动

- 4.1 前端vue开发环境安装
  ```
    *如果你不懂，实在找不到学习vue的视频，那再联系我把，我有几吨免费的vue课程，对了，我还有永久激活idea的插件
    *推荐使用idea,因为在idea最下面工具栏点Terminal,就可以操作dos命令了
    1. 安装nodejs的开发环境(不会的话百度吧)
    2. 安装npm的淘宝镜像（一定要用淘宝镜像，不然有的依赖下载不完！！启动不了）
    3. 使用淘宝镜像安装依赖 cnpm install(第一次要执行这个命令下载node_modules,后面修改代码后用npm run build 就可以啦)
    4. 启动 npm run dev (会生成dist文件夹,服务器不属实可以把他丢到服务器上，nginx指向他就能访问啦)
  ```

- 4.2.前端小程序开发环境安装
  ```
    使用“微信开发者工具” 导入项目，设置你小程序申请的appid(和后端使用大小程序id保持一致)打开项目。然后就可以用了
  ```
  
- 4.3vue后台`monkey-shop-admin中WebApplication` 启动
   ```
    application.yml  
        >  active: dev,quartz  #生产就不能配置dev了，换成prod
    application-dev.yml   #本地启动配置文件，修改数据库信息
        > port: 8085   
        >url: jdbc:mysql://ip:3306/monkey_shop_test?allowMultiQueries=true&useSSL=false&useUnicode=true&characterEncoding=UTF-8&autoReconnect=true&zeroDateTimeBehavior=convertToNull&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=GMT%2B8&nullCatalogMeansCurrent=true
        >username: root
        >password: 123456
    补充：swagger文档路径 localhost:8085/doc.html
  ```
- 4.4小程序后台通过`monkey-shop-api中ApiApplication` 启动
  ```
    同上
    补充：swagger文档路径 localhost:8086/apis/doc.html
  ```
  ```
    注意：当idea启动项里没有以上两个启动项时:
    ① 请看下jdk忘了选 
    ② 看下ieda又上角是否有Maven选项卡,
    没有的话找到项目根目录pom.xml,右键maven import
  ```
#5. 关于CentOS服务器环境安装
### 5.1 安装jdk
####方式一：
  ```
    #安装openJDK,如果没有java-1.8.0-openjdk-devel就没有javac命令
    yum  install  java-1.8.0-openjdk   java-1.8.0-openjdk-devel
  ```
####方式二:
   ```
    #安装官网下载的jdk版本
    #查看系统是否自带JDK:
    java -version
    #有版本号则查看文件： 
    rpm -qa | grep java
    #su root 输入密码获取root权限，删除上面显示的java相关的jdk文件，如:
    rpm -e --nodeps java-1.8.0-openjdk-headless-1.8.0.65-3.b17.el7.x86_64
    rpm -e --nodeps java-1.8.0-openjdk-1.8.0.65-3.b17.el7.x86_64
    #清理完之后安装jdk:
    #下载官网http://www.oracle.com/technetwork/java/javase/downloads/index.html下载jdk
    #上传到服务器目录/usr 
    #解压
    tar -zxvf 下载的.gz文件
    #配置环境变量
    vim /etc/profile
    #按键盘ins 键 加入一下内容，目录要换成自己的
    export JAVA_HOME=/usr/jdk1.8.0_211
    export CLASSPATH=.:${JAVA_HOME}/jre/lib/rt.jar:${JAVA_HOME}/lib/dt.jar:${JAVA_HOME}/lib/tools.jar
    export PATH=$PATH:${JAVA_HOME}/bin
    #按esc键
    #然后按冒号输入以下命令回车
    wq! 
    #执行一下命令让修改生效即完成jdk安装全部步骤
    source /etc/profile
   ``` 
###5.2 安装mysql
#### 1、配置YUM源
   ``` 
    #在[MySQL]官网中下载YUM源rpm安装包：http://dev.mysql.com/downloads/repo/yum/ 
    #下载mysql源安装包
    shell> wget http://dev.mysql.com/get/mysql57-community-release-el7-8.noarch.rpm
    #安装mysql源 
    shell> yum localinstall mysql57-community-release-el7-8.noarch.rpm
    #检查mysql源是否安装成功
    shell> yum repolist enabled | grep "mysql.*-community.*"
   ``` 
#### 2、安装MySQL
   ``` 
    #yum安装
    shell>  yum install mysql-community-server
    # 配置默认编码为utf8 并且设置不区分大小写
    #修改/etc/my.cnf配置文件，在[mysqld]下添加编码配置，如下所示：
    [mysqld]
    character_set_server=utf8
    init_connect='SET NAMES utf8'
    lower_case_table_names=1
    # 启动MySQL服务
    shell>  systemctl start mysqld
    # 设置开机启动,避免服务重启后数据库没启动情况发生
    shell>  systemctl enable mysqld 
    shell>  systemctl daemon-reload
    # 修改root默认密码
    #mysql安装完成之后，在/var/log/mysqld.log文件中给root生成了一个默认密码。通过下面的方式找到root默认密码，然后登录mysql进行修改：
    shell> grep 'temporary password' /var/log/mysqld.log
    
    #查看到密码后用root登录修改密码
    
    shell> mysql -uroot -p 密码
    
    mysql> ALTER USER 'root'@'localhost' IDENTIFIED BY '你的密码'; 
    #或者
    mysql> set password for 'root'@'localhost'=password('你的密码'); 
    
    #密码规则：大小写字母、数字和特殊符号，并且长度不能少于8位
    #创建数据库并添加远程登录用户
    #默认只允许root帐户在本地登录，如果要在其它机器上连接mysql，必须修改root允许远程连接，或者添加一个允许远程连接的帐户，为了安全起见，我添加一个新的帐户：
   ``` 

####5.3 安装redis
   ``` 
    ###需要先安装tcl 
    shell> wget http://downloads.sourceforge.net/tcl/tcl8.6.8-src.tar.gz
    shell> tar xzvf tcl8.6.8-src.tar.gz -C /usr/local/
    shell> cd  /usr/local/tcl8.6.8/unix/
    shell> ./configure  
    shell> make && make install
    
    ###安装redis
    shell> wget http://download.redis.io/releases/redis-4.0.11.tar.gz
    shell> tar xzvf redis-4.0.11.tar.gz -C /usr/local/
    shell> cd  /usr/local/redis-4.0.11/
    shell> make && make test && make install
    
    # redis的生产环境启动方案
    要把redis作为一个系统的daemon进程去运行的，每次系统启动，redis进程一起启动
    
    1. wget下载redis解压出来的文件夹里面有个utils，utils目录下有个redis_init_script脚本
    2. 将redis_init_script脚本拷贝到linux的/etc/init.d目录中，将redis_init_script重命名为redis_6379，6379是我们希望这个redis实例监听的端口号
    3. 修改redis_6379脚本的第6行的REDISPORT，设置为相同的端口号（默认就是6379）
    4. 创建两个目录：/etc/redis（存放redis的配置文件），/var/redis/6379（存放redis的持久化文件）
    5. 修改redis配置文件（默认在根目录下，redis.conf），拷贝到/etc/redis目录中，修改名称为6379.conf
    6. 修改redis.conf中的部分配置为生产环境
    
        daemonize	yes							让redis以daemon进程运行
        pidfile		/var/run/redis_6379.pid 	设置redis的pid文件位置
        port		6379						设置redis的监听端口号
        dir 		/var/redis/6379				设置持久化文件的存储位置
    
    #1. 启动redis，执行
    shell> cd /etc/init.d
    shell> chmod 777 redis_6379
    shell> ./redis_6379 start
    #2. 确认redis进程是否启动，ps -ef | grep redis
    #3. 让redis跟随系统启动自动启动
    在redis_6379脚本中，最上面，加入两行注释
    # chkconfig:   2345 90 10
    # description:  Redis is a persistent key-value database
    执行
    chkconfig redis_6379 on
    ## redis cli的使用
    shell> redis-cli SHUTDOWN，连接本机的6379端口停止redis进程
    shell> redis-cli -h 127.0.0.1 -p 6379 SHUTDOWN，制定要连接的ip和端口号
    shell> redis-cli PING，ping redis的端口，看是否正常
    shell> redis-cli 进入交互式命令行
    如果需要redis客户端，请移步三只猴子官方QQ群(1121081307)文件中自由下载 
   ``` 

#### 5.4 安装nginx
######需要用nginx原因：解决跨域问题,小程序需要用https访问，分布式部署，负载均衡，代理服务器转发
   ``` 
    yum install -y nginx
    
    ## 2、启动Nginx并设置开机自动运行
    shell> systemctl start nginx.service
    shell> systemctl enable nginx.service
    
    ## 3、配置nginx
    shell> vi /etc/nginx/nginx.conf
    
    配置示例：
    #小程序接口的域名配置，小程序规定要https，填写对应域名，并把https证书上传至服务器
    #后台域名配置，后台vue页面代码上传至 /usr/share/nginx/admin
    orker_processes  1;
    pid logs/nginx.pid;
    events {
        worker_connections  1024;
    }
    
    http {
        include       mime.types;
        default_type  application/octet-stream;
    
        sendfile        on;
        tcp_nopush     on;
    
        keepalive_timeout  65;
    
        server {
              listen 80;
              server_name www.3monkeys.shop 3monkeys.shop;
              location / {
                  root /app/org/kawaii/kawaii;
                  index index.html index.htm;
              }
        }
    
        server {
             listen       80; 
             server_name  admin.3monkeys.shop www.admin.3monkeys.shop;
             root /app/vue/dist; #vue  npm run build 之后生成的文件夹
             location / {
                #root /app/vue/dist;
                #index index.html index.htm;
             }
    
             location /apis {
                 #这里apis是前端vue中设置了代理
                 rewrite  ^/apis/(.*)$ /$1 break;
                 proxy_pass   http://127.0.0.1:8085;
             }
        }
    
        server {
            listen 443 ssl;
            #server_name www.applet.3monkeys.shop;
            server_name www.applet.3monkeys.shop applet.3monkeys.shop;
            #ssl on;
            #下面两行是ssl证书(.pem和.key)，获取方式阿里云一年免费，可申请多个，在阿里云控制台ssl证书管理页面绑定好域名，点击验证后即可下载证书，保存到服务器即可
            ssl_certificate  /usr/local/nginx/conf/cert/4160096_www.applet.3monkeys.shop.pem;
            ssl_certificate_key /usr/local/nginx/conf/cert/4160096_www.applet.3monkeys.shop.key;
            ssl_session_timeout 5m;
            ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
            ssl_ciphers ALL:!ADH:!EXPORT56:RC4+RSA:+HIGH:+MEDIUM:+LOW:+SSLv2:+EXP;
            ssl_prefer_server_ciphers on;
            location / {
              proxy_pass http://127.0.0.1:8086;
              #proxy_set_header Host $host;
              #proxy_set_header X-Real-IP $remote_addr;
              #proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
              #root /app/vue/dist;
              #index index.html index.htm;
            }
         }
    }
    #修改之后重启生效
    shell > ./nginx -s reload
    #备注:
    #多个域名可以公用80端口，先通过server_name(可配置多个用空格隔开)过滤，匹配到不同的server，当匹配不到的时候就会默认最上面的。
    http:xxxx.com  访问默认80端口
    https:xxxx.com 访问吗欧仁443端口
    rewrite  ^/notice/(.*)$ /$1 break;  解决跨越
    proxy_pass http://127.0.0.1:8086;   后台跳转的地址
    #关于域名购买，域名解析，域名备案，服务器购买，ssl购买，端口开放等阿里云官方有知道步骤，如有不清楚可以三只猴子官方群免费提供帮助
    
    ## 4、重启nginx，让配置生效
    shell> systemctl restart nginx.service
   ``` 
 #6. 关于服务部署
 ###6.1 部署前端vue
   ``` 
       还记得之前将的 npm run build 构建之后生产的dist夹吧，上传到服务器上，
       然后nginx指向他就好了，怎么指向请看上面nginx配置
   ``` 
  ###6.2 部署后端服务jar
   ``` 
    - 修改完毕后打包，使用`mvn clean package -DskipTests`  命令进行打包，最终会在target文件夹中生成很多的jar，我们需要其中两个。
    - 或者idea右上角点（第一次要父级项目全局install下）maven > monkey-shop > Lifecycle > clean > install
    - 商城后台接口 `monkey-shop-admin\target\monkey-shop-admin-0.0.1-SNAPSHOT.jar`
    - 商城前端接口`monkey-shop-api\target\monkey-shop-api-0.0.1-SNAPSHOT.jar`
    
    #上传jar到服务器/app/api   和  /app/admin 后部署项目
    #测试环境：
    shell> nohup java -jar /app/api/monkey-shop-api-0.0.1-SNAPSHOT.jar
    shell> nohup java -jar /app/admin/monkey-shop-admin-0.0.1-SNAPSHOT.jar
    #查看日志切换到对应目录查看nohup.out
    #实时查看
    shell> tail -f nohup.out
    #查看最近10行
    shell> tail -10f nohup.out
    #可以尝试more nohup.out /  less nohup.out 后按回车和空格键试试
    #
    生成环境：
    shell> nohup java -jar -Dspring.profiles.active=prod monkey-shop-api-0.0.1-SNAPSHOT.jar > /app/api/log/api-console.log" &
    #admin包有定时任务选项
    shell> nohup java -jar -Dspring.profiles.active=prod,quartz /app/admin/monkey-shop-admin-0.0.1-SNAPSHOT.jar >/app/admin/log/admin-console.log" &
    启动好之后nginx配置请移步上面nginx安装里面哦
   ``` 
 # 7. ```当你看到这的时候，我只能说老铁是个狠人,看你骨骼惊奇，很欣赏你,到这里如果还有没有理解的请加qq群1121081307，与各位高手交流交流经验吧!```