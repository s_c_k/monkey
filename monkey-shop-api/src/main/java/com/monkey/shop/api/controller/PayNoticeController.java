/*
 * Copyright (c) 2018-2999 武汉三只猴子科技有限公司 All rights reserved.
 *
 * https://www.3monkeys.shop/
 *
 * 未经允许，不可做商业用途！
 *
 * 版权所有，侵权必究！
 */

package com.monkey.shop.api.controller;

import com.github.binarywang.wxpay.bean.notify.WxPayOrderNotifyResult;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.github.binarywang.wxpay.service.WxPayService;
import com.monkey.shop.common.sdk.HttpKit;
import com.monkey.shop.common.sdk.PaymentKit;
import com.monkey.shop.service.PayService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.extern.slf4j.XSlf4j;
import org.slf4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@Slf4j
@ApiIgnore
@RestController
@RequestMapping("/notice/pay")
@AllArgsConstructor
public class PayNoticeController {
    //private static final Logger log = Logger.getLogger(PayNoticeController.class);
    /**
     * 小程序支付
     */
    private final WxPayService wxMiniPayService;

    private final PayService payService;
   // 成功的标识
   private final static String SUCCESS = "SUCCESS";


    @RequestMapping("/order")
    public ResponseEntity<Void> submit(@RequestBody String xmlData) throws WxPayException {
        WxPayOrderNotifyResult parseOrderNotifyResult = wxMiniPayService.parseOrderNotifyResult(xmlData);
       log.info("触发支付回调方法:",xmlData);
        String payNo = parseOrderNotifyResult.getOutTradeNo();
        String bizPayNo = parseOrderNotifyResult.getTransactionId();
        // 根据内部订单号更新order settlement
        payService.paySuccess(payNo, bizPayNo);
        log.info("触发支付回调方法:{}，{}",payNo,bizPayNo);
        return ResponseEntity.ok().build();
    }

   @ApiOperation("支付回调")
   @PostMapping("/order1")
   public void payNotify(HttpServletRequest request, HttpServletResponse response) throws Exception {
      log.info("新支付回调1");
      String xmlMsg = HttpKit.readData(request);
      log.info("新支付回调2:{}",xmlMsg);
      WxPayOrderNotifyResult parseOrderNotifyResult = wxMiniPayService.parseOrderNotifyResult(xmlMsg);

      String payNo = parseOrderNotifyResult.getOutTradeNo();
      String bizPayNo = parseOrderNotifyResult.getTransactionId();
      log.info("新支付回调3:payNo{}bizPayNo{}",payNo,bizPayNo);
      String result = "<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>";
      try {
         response.getWriter().write(result);
      } catch (IOException e) {
         e.printStackTrace();
      }
      // 根据内部订单号更新order settlement
      payService.paySuccess(payNo, bizPayNo);
      log.info("新支付回调4:result{}",result);
   }
}