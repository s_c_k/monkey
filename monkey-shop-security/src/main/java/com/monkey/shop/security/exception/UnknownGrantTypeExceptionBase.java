/*
 * Copyright (c) 2018-2999 武汉三只猴子科技有限公司 All rights reserved.
 *
 * https://www.3monkeys.shop/
 *
 * 未经允许，不可做商业用途！
 *
 * 版权所有，侵权必究！
 */

package com.monkey.shop.security.exception;

import org.springframework.http.HttpStatus;

public class UnknownGrantTypeExceptionBase extends BaseMonkeyAuth2Exception {

    public UnknownGrantTypeExceptionBase(String msg) {
        super(msg);
    }


    public UnknownGrantTypeExceptionBase(String msg, Throwable t) {
        super(msg);
    }

    @Override
    public String getOAuth2ErrorCode() {
        return "unknown_grant_type";
    }

    @Override
    public int getHttpErrorCode() {
        return HttpStatus.UNAUTHORIZED.value();
    }

}