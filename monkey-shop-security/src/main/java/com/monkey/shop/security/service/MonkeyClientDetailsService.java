/*
 * Copyright (c) 2018-2999 武汉三只猴子科技有限公司 All rights reserved.
 *
 * https://www.3monkeys.shop/
 *
 * 未经允许，不可做商业用途！
 *
 * 版权所有，侵权必究！
 */

package com.monkey.shop.security.service;

import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;

import javax.sql.DataSource;

/**
 * @author
 * @tate 2019/03/30
 * 获取客户端
 */
public class MonkeyClientDetailsService extends JdbcClientDetailsService {

	public MonkeyClientDetailsService(DataSource dataSource) {
		super(dataSource);
	}
}
