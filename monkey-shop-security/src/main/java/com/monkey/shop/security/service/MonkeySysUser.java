/*
 * Copyright (c) 2018-2999 武汉三只猴子科技有限公司 All rights reserved.
 *
 * https://www.3monkeys.shop/
 *
 * 未经允许，不可做商业用途！
 *
 * 版权所有，侵权必究！
 */

package com.monkey.shop.security.service;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

/**
 * 用户详细信息
 *
 * @author
 */
@Getter
@Setter
public class MonkeySysUser extends User {
	/**
	 * 用户ID
	 */
	@Getter
	private Long userId;

	/**
	 * 租户ID
	 */
	@Getter
	private Long shopId;

	public MonkeySysUser(Long userId, Long shopId, String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		this.userId = userId;
		this.shopId = shopId;
	}
}
