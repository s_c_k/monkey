/*
 * Copyright (c) 2018-2999 武汉三只猴子科技有限公司 All rights reserved.
 *
 * https://www.3monkeys.shop/
 *
 * 未经允许，不可做商业用途！
 *
 * 版权所有，侵权必究！
 */

package com.monkey.shop.bean.enums;


/**
 * 运费收费方式 （0 按件数,1 按重量 2 按体积）
 * @author zkk
 */
public enum TransportChargeType {

    /**
     * 0全部商品参与
     */
    COUNT(0),

    /**
     * 1指定商品参与
     */
    WEIGHT(1),

    /**
     * 2指定商品不参与
     */
    VOLUME(2)
    ;

    private Integer num;

    public Integer value() {
        return num;
    }

    TransportChargeType(Integer num){
        this.num = num;
    }

    public static TransportChargeType instance(Integer value) {
        TransportChargeType[] enums = values();
        for (TransportChargeType statusEnum : enums) {
            if (statusEnum.value().equals(value)) {
                return statusEnum;
            }
        }
        return null;
    }
}
