/*
 * Copyright (c) 2018-2999 武汉三只猴子科技有限公司 All rights reserved.
 *
 * https://www.3monkeys.shop/
 *
 * 未经允许，不可做商业用途！
 *
 * 版权所有，侵权必究！
 */

package com.monkey.shop.bean;

import java.util.ArrayList;
import java.util.List;

import com.monkey.shop.bean.bo.SmsInfoBo;

import cn.hutool.core.collection.CollectionUtil;

public class SmsInfoContext {

	/** The request holder. */
	private static ThreadLocal<List<SmsInfoBo>> smsInfoHolder = new ThreadLocal<List<SmsInfoBo>>();


	public static List<SmsInfoBo> get(){
		List<SmsInfoBo> list = smsInfoHolder.get();
		if (CollectionUtil.isEmpty(list)) {
			return new ArrayList<>();
		}
		return smsInfoHolder.get();
	}
	
	public static void set(List<SmsInfoBo> smsInfoBos){
		 smsInfoHolder.set(smsInfoBos);
	}
	
	public static void put(SmsInfoBo smsInfoBo){
		List<SmsInfoBo> smsInfoBos = smsInfoHolder.get();
		if (CollectionUtil.isEmpty(smsInfoBos)) {
			smsInfoBos = new ArrayList<>();
		}
		smsInfoBos.add(smsInfoBo);
		smsInfoHolder.set(smsInfoBos);
	}
	
	public static void clean() {
		if (smsInfoHolder.get() != null) {
			smsInfoHolder.remove();
		}
	}
}