package com.monkey.shop.admin.security;

import com.monkey.shop.security.service.MonkeySysUser;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * token增强
 * @author zkk
 */
@Component
public class AdminTokenEnhancer implements TokenEnhancer {


    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        final Map<String, Object> additionalInfo = new HashMap<>(8);
        MonkeySysUser monkeySysUser = (MonkeySysUser) authentication.getUserAuthentication().getPrincipal();
        additionalInfo.put("shopId", monkeySysUser.getShopId());
        additionalInfo.put("userId", monkeySysUser.getUserId());
        additionalInfo.put("authorities", monkeySysUser.getAuthorities());
        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
        return accessToken;
    }
}
