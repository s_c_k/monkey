/*
 * Copyright (c) 2018-2999 武汉三只猴子科技有限公司 All rights reserved.
 *
 * https://www.3monkeys.shop/
 *
 * 未经允许，不可做商业用途！
 *
 * 版权所有，侵权必究！
 */

package com.monkey.shop.service;

import com.monkey.shop.bean.app.param.PayParam;
import com.monkey.shop.bean.pay.PayInfoDto;

import java.util.List;

/**
 * @author zkk on 2018/09/15.
 */
public interface PayService {


    PayInfoDto pay(String userId, PayParam payParam);

    List<String> paySuccess(String payNo, String bizPayNo);

}
